using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD6_7
{
    class StringDigitChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            char[] letter  = stringToCheck.ToCharArray();
            for(int i = 0; i < stringToCheck.Length; i++)
            {
                if (letter[i] >= '0' && letter[i] <= '9')
                {
                    return true;
                }
            }
            return false;
        }
    }
}
