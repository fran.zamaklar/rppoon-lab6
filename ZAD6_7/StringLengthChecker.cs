using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD6_7
{
    class StringLengthChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            return stringToCheck.Length > 4;
        }
    }
}
