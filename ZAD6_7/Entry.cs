using System;

namespace ZAD6_7
{
    class Entry
    {
        static void Main(string[] args)
        {
            //ZAD 6

            StringChecker stringChecker1 = new StringLengthChecker();
            StringChecker stringChecker2 = new StringDigitChecker();
            StringChecker stringChecker3 = new StringLowerCaseChecker();
            StringChecker stringChecker4 = new StringUpperCaseChecker();
            stringChecker1.SetNext(stringChecker2);
            stringChecker2.SetNext(stringChecker3);
            stringChecker3.SetNext(stringChecker4);

            string pass1 = "IvanGundulic1854";
            string pass2 = "oSmaN";
            string pass3 = "1245";

            Console.WriteLine("Ispravnost lozinke 1 : " + stringChecker1.Check(pass1));
            Console.WriteLine("Ispravnost lozinke 2  : " + stringChecker1.Check(pass2));
            Console.WriteLine("Ispravnost lozinke 3 : " + stringChecker1.Check(pass3));
            Console.WriteLine();

            //ZAD 7 

            PasswordValidator validator = new PasswordValidator(new StringLengthChecker());
            validator.AddChecker(new StringDigitChecker());
            validator.AddChecker(new StringUpperCaseChecker());
            validator.AddChecker(new StringLowerCaseChecker());

            string password1 = "IvAnGuNdUliC1854";
            string password2 = "Osman2";
            string password3 = "2455";

            Console.WriteLine("Ispravnost lozinke 1 : " + validator.Check(password1));
            Console.WriteLine("Ispravnost lozinke 2  : " + validator.Check(password2));
            Console.WriteLine("Ispravnost lozinke 3 : " + validator.Check(password3));
        }
    }
}
