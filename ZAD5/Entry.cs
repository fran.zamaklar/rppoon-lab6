using System;

namespace ZAD5
{
    class Entry
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, @"C:\Users\Lenovo\Documents\logFile.txt");
            logger.SetNextLogger(fileLogger);
            logger.Log("Sada me vidis", MessageType.ERROR);
            logger.Log("Sada me ne vidis", MessageType.INFO);
            logger.Log("Sada me opet ne vidis", MessageType.WARNING);
        }
    }
}
