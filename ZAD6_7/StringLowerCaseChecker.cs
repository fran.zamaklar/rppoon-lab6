using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD6_7
{
    class StringLowerCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            char[] letter = stringToCheck.ToCharArray();
            for (int i = 0; i < stringToCheck.Length; i++)
            {
                if (letter[i] >= 'a' && letter[i] <= 'z')
                {
                    return true;
                }
            }
            return false;
        }
    }
}
