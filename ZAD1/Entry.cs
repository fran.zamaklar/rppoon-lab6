using System;

namespace ZAD1
{
    class Entry
    {
        static void Main(string[] args)
        {
            Note note1 = new Note("Prva zabiljeska", "Pokreni se");
            Note note2 = new Note("Druga zabiljeska", "Ugasi se");
            Note note3 = new Note("Treca zabiljeska", "Zamrzni se");
            Notebook notebook = new Notebook();
            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.AddNote(note3);
            IAbstractIterator iterator = notebook.GetIterator();
            for(Note note = iterator.First(); iterator.IsDone == false; note = iterator.Next())
            {
                note.Show();
            }
            
        }
    }
}
