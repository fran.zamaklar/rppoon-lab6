using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD3
{
    class CareTaker
    {
        List<Memento> saves;
        public Memento PreviousState { get; set; }
        public CareTaker()
        {
            saves = new List<Memento>();
        }
        public void addSave(Memento save)
        {
            saves.Add(save);
        }

        public void removeSave(int index)
        {
            saves.RemoveAt(index);
        }

        public void clearSaves()
        {
            saves.Clear();
        }

        public Memento getSave(int index)
        {
            return saves[index];
        }
    }
}
