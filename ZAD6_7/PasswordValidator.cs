using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD6_7
{
    class PasswordValidator
    {
        private StringChecker first;
        private StringChecker last;

        public PasswordValidator(StringChecker stringchecker)
        {
            this.first = stringchecker;
            this.last = stringchecker;
        }

        public void AddChecker(StringChecker stringchecker)
        {
            this.last.SetNext(stringchecker);
            this.last = stringchecker;
        }

        public bool Check(string password)
        {
            return first.Check(password);
        }
    }
}
