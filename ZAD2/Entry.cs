using System;

namespace ZAD2
{
    class Entry
    {
        static void Main(string[] args)
        {
            Box box = new Box();
            Product product1 = new Product("Mlijeko", 10);
            Product product2 = new Product("Slanina", 30);
            Product product3 = new Product("Sir", 7);
            box.AddProduct(product1);
            box.AddProduct(product2);
            box.AddProduct(product3);
            IAbstractIterator iterator = box.GetIterator();
            for(Product product = iterator.First(); iterator.IsDone == false; product = iterator.Next())
            {
                Console.WriteLine(product.ToString());
            }
        }
    }
}
