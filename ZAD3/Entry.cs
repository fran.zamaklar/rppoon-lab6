using System;

namespace ZAD3
{
    class Entry
    {
        static void Main(string[] args)
        {
            CareTaker careTaker = new CareTaker();
            ToDoItem item = new ToDoItem("Podsjetnik", "Operi prozore", new DateTime(2020, 5, 20));
            careTaker.addSave(item.StoreState());
            item.ChangeTask("Usisaj");
            careTaker.addSave(item.StoreState());
            item.ChangeTask("Obrisi prasinu");
            careTaker.addSave(item.StoreState());
            item.RestoreState(careTaker.getSave(2));
            Console.WriteLine(item.ToString());
        }
    }
}
