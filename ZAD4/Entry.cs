using System;
using System.Collections.Generic;

namespace ZAD4
{
    class Entry
    {
        static void Main(string[] args)
        {
            BankAccount bankAccount = new BankAccount("Una", "Stjepana Radica 2", (decimal)15059.2);
            List<Memento> careTaker = new List<Memento>();
            careTaker.Add(bankAccount.Store());
            bankAccount.ChangeOwnerAddress("Trg Ante Starcevica 102");
            careTaker.Add(bankAccount.Store());
            bankAccount.Restore(careTaker[1]);
            Console.WriteLine(bankAccount.ToString());
        }
    }
}
