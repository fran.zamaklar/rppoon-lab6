using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD4
{
    class Memento
    {
        public string Name { get; private set; }
        public string Adress { get; private set; }
        public decimal Balance { get; private set; }
        public Memento(string name, string adress, decimal balance)
        {
            this.Name = name;
            this.Adress = adress;
            this.Balance = balance;
        }
    }
}
