using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD2
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
